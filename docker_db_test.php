<?php

try {
    $dbh = new PDO('mysql:host=localhost;dbname=yii2advanced_test', 'root', '');
    foreach($dbh->query('SHOW TABLES') as $row) {
        print_r($row);
    }
    $dbh = null;
} catch (PDOException $e) {
    print 'Error!: ' . $e->getMessage() . "<br/>";
}


try {
    $dbh = new PDO('mysql:host=localhost;dbname=yii2advanced_test', 'root', 'test');
    foreach($dbh->query('SHOW TABLES') as $row) {
        print_r($row);
    }
    $dbh = null;
} catch (PDOException $e) {
    print 'Error!: ' . $e->getMessage() . "<br/>";
}

try {
    $dbh = new PDO('mysql:host=127.0.0.1;dbname=yii2advanced_test', 'root', 'test');
    foreach($dbh->query('SHOW TABLES') as $row) {
        print_r($row);
    }
    $dbh = null;
} catch (PDOException $e) {
    print 'Error!: ' . $e->getMessage() . "<br/>";
}